<!--- Local IspellDict: de -->
<!-- SPDX-FileCopyrightText: 2019-2020 Jens Lechtenbörger -->
<!-- SPDX-License-Identifier: CC0-1.0 -->

[OER (Präsentationen und weitere Materialien)](https://oer.gitlab.io/oer-courses/vm-oer)
für ein Vertiefungsmodul an der WWU Münster rund um
[OER](https://de.wikipedia.org/wiki/Open_Educational_Resources),
zunächst im Wintersemester 2019/2020, dann wiederholt in 2020/2021.
