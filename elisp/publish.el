;;; publish.el --- Publish reveal.js presentations, HTML, and PDF from Org sources
;; -*- Mode: Emacs-Lisp -*-
;; -*- coding: utf-8 -*-

;; SPDX-FileCopyrightText: 2017-2020 Jens Lechtenbörger
;; SPDX-License-Identifier: GPL-3.0-or-later

;;; Commentary:
;; Mostly, settings from emacs-reveal-publish are used.
;; Besides, non-free logos are published here.

;;; Code:
;; Avoid update of emacs-reveal, enable stacktraces.
(setq emacs-reveal-managed-install-p nil
      debug-on-error t)

;; Set up load-path.
(let ((install-dir
       (mapconcat #'file-name-as-directory
                  `(,user-emacs-directory "elpa" "emacs-reveal") "")))
  (add-to-list 'load-path install-dir)
  (condition-case nil
      ;; Either require package with above hard-coded location
      ;; (e.g., in docker) ...
      (require 'emacs-reveal)
    (error
     ;; ... or look for sibling "emacs-reveal".
     (add-to-list
      'load-path
      (expand-file-name "../../../emacs-reveal/" (file-name-directory load-file-name)))
     (require 'emacs-reveal))))

;; Use local klipse-libs for offline use.
(add-to-list 'oer-reveal-plugins "klipse-libs")

;; In this course, we prefer to see alternate type links even for text documents.
(setq oer-reveal-with-alternate-types '("org" "pdf"))

(oer-reveal-publish-all
 (list
  (list "pdf-index"
       	:base-directory "."
	:include '("index.org")
	:exclude ".*"
       	:publishing-function '(org-latex-publish-to-pdf)
       	:publishing-directory "./public")
  (list "texts"
       	:base-directory "texts"
       	:base-extension "org"
        :exclude "config"
        :html-postamble ""
       	:publishing-function '(oer-reveal-publish-to-html
                               org-latex-publish-to-pdf)
       	:publishing-directory "./public/texts")
  (list "images"
	:base-directory "img"
	:base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	:publishing-directory "./public/img"
	:publishing-function 'org-publish-attachment)
  (list "prog-imgs"
        :base-directory "programming/texts"
        :base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
        :publishing-function 'org-publish-attachment
        :publishing-directory "./public/programming/texts")
  (list "prog-quizzes"
	:base-directory "programming/quizzes"
	:base-extension (regexp-opt '("js"))
	:publishing-directory "./public/quizzes"
	:publishing-function 'org-publish-attachment)
  (list "title-logos"
	:base-directory "non-free-logos/title-slide"
	:base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	:publishing-directory "./public/title-slide"
	:publishing-function 'org-publish-attachment)
  (list "theme-logos"
	:base-directory "non-free-logos/reveal-css"
	:base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	:publishing-directory "./public/reveal.js/css/theme"
	:publishing-function 'org-publish-attachment)))

(provide 'publish)
;;; publish.el ends here
